package com.ucbcba.proyecto.proyecto.Services;

import com.ucbcba.proyecto.proyecto.Entities.Pedido;

import java.util.ArrayList;

public interface PedidoService {
    Iterable<Pedido> listAllPedidos();
    Pedido getPedidoById(Integer id);

    Pedido savePedido(Pedido pedido);

    long cantidadPedidos();

    Pedido getLast();
}
